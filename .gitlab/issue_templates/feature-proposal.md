
## This template is for requesting new or updated features.

This repository primarily relates to the logged out [marketing website](https://about.gitlab.com/) and [company handbook](https://about.gitlab.com/handbook/). This is not the right repository for requests related to docs.gitlab.com, product, or other parts of the site.

# Issues should identify [The Five W's](https://en.wikipedia.org/wiki/Five_Ws) : who, what, when, where, and why.

It is also recommended to apply [MoSCoW](https://en.wikipedia.org/wiki/MoSCoW_method) sorting to your requests (must, should, could, won't).

#### Please apply appropriate labels or your issue may not be sorted on to appropriate boards.

Please refer to the [website label documentation](/handbook/marketing/website/team-documentation/#issue-labels)

## Please delete the lines above and any irrelevant sections below.

Below are suggestions. Any extra information you provide is beneficial.

#### Briefly describe the feature being proposed

(Example: Add a dropdown menu)

#### Describe the relevant location and pages

(Example: Main site navigation on every page)

#### What problem is this feature trying to solve?

(Example: Increase EE trials)

#### If applicable, do you have any user journeys, wireframes, prototypes, content/data?

(Example: Google doc/sheet, balsamiq, etc)

### Who is the primary audience? Be specific.

(Example: Potential customers vs developers vs CTO vs Jenkins users)

If known, include any of the following: types of users (e.g. Developer), personas, or specific company roles (e.g. Release Manager). It's okay to write "Unknown" and fill this field in later. Personas can be found at [https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/)

#### Any other audiences?

(Example: Media outlets, investors, etc)

#### What are the KPI (Key Performance Indicators)

(Example: Premium signups per month as identified by Periscope dashboard URL)

#### Will this change require any new or updated events, parameters, or tracking?

(Example: Clicking button X should record a purchase into analytics)

#### Will this require any new or updated reports?

(Example: This impacts the Retention dashboard in Periscope)

#### Will this require any new or updated automation?

(Example: Create a test ensuring a job listing is never empty)

<!-- These labels will be automatically applied unless you edit or delete the following section -->
/label ~"mktg-website" ~"template::web-feature-proposal" ~"mktg-status::triage"

<!-- If you do not want to ping the website team, please remove the following section -->
/cc @gl-website
