---
layout: handbook-page-toc
title: "Deal Desk"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to The Deal Desk Handbook 

### Charter

The Deal Desk team's mission is to streamline the opportunity management process while acting as a trusted business partner for field sales. We are the first point of contact for sales support.

### Key Focus Areas
    
*  Sales Support
*  Quote Configuration
*  Month End / Quarter End Reconciliation

### Helpful Links

*   **Salesforce Reports and Dashboards**
    *   [FY20 Q4 WW Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M0000007H7W)
    *   [Monthly Bookings Report](https://gitlab.my.salesforce.com/00O61000004Ik27)
    *   [Deal Desk Pending Opportunity Approvals Report](https://gitlab.my.salesforce.com/00O4M000004e0Dp)

*   **Frequently Used Handbook Pages**
    *   [Sales Order Processing](https://about.gitlab.com/handbook/business-ops/order-processing/)
    *   [Deal Desk Opportunity Approval Process](https://about.gitlab.com/handbook/business-ops/order-processing/#submitting-an-opportunity-for-deal-desk-approval)
    *   [Useful Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information)
    *   [Account Ownership Rules of Engagement](https://about.gitlab.com/handbook/business-ops/resources/#account-ownership-rules-of-engagement)
    *   [IACV Calculation Guide](https://about.gitlab.com/handbook/sales/#incremental-annual-contract-value-iacv)
    *   [Vendor Setup Form Process](https://about.gitlab.com/handbook/business-ops/order-processing/#how-to-process-customer-requested-vendor-setup-forms)
    *   [Security Questionnaire Process](https://about.gitlab.com/handbook/engineering/security/#process)
    *   [Troubleshooting: True Ups, Licenses + EULAS](https://about.gitlab.com/handbook/business-ops/business_systems/portal/troubleshooting/)
    *   [Licensing FAQ](https://about.gitlab.com/pricing/licensing-faq/)
    *   [Legal Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/)
    *   [Trade Compliance (Export/Import)](https://about.gitlab.com/handbook/business-ops/order-processing/#trade-compliance-export--import-and-visual-compliance-tool-in-salesforce)

*   **Other Resources**
    *   [Sales Territory Spreadsheet](https://docs.google.com/spreadsheets/d/1PYU8oQJQEPpi8K-SHuqSgPeSpLcWeSQd9FuwKtgD048/edit?ts=5d6ea274#gid=0)
    *   [Quote Approval Matrix](https://docs.google.com/document/d/1-CH-uH_zr0qaVaV1QbmVZ1rF669DsaUeq9w-q1QiKPE/edit?ts=5d6ea430#heading=h.ag75fqu12pf0)
    *   [Billing FAQs and Useful Tips](https://gitlab.com/gitlab-com/finance/-/wikis/Billing-Team-FAQs-&-Useful-Tips)

### Sales Support 

##### Deal Desk SLA 

The Deal Desk team will do their best to respond to each request within 4 hours. Revenue generating, current quarter requests will take priority, especially during Month & Quarter End. If a task is not resolved within 24 hours it will be escalated (if necessary). 

| Type of Request | First Response | Resolution |
|----- | ----- | ------| 
| Basic Quote Assistance | 6 Hours | 8 Hours | 
| Ramp Deal | 6 Hours | 24 hours |
| Flat Renewal | 6 Hours | 24 Hours |
| IACV Calculation | 6 Hours | 24 Hours |
| Contract Reset / Co-Term | 6 Hours | 24 Hours | 
| RFP/Vendor Forms | 6 Hours | Dependent on AM |

##### Deal Desk - A Global Presence

The Deal Desk team is located around the world and will be available during standard business hours within most regions. 

**EMEA**
*  Meri Gil Galindo - Dublin, Ireland
*  Marcsi Szucs - Budapest, Hungary

 **AMER**
*  Jesse Rabbits - New York, NY 
*  Cal Baker - Seattle, WA

**APAC**
*  Kriti D'Souza - Pune, India 

##### Salesforce Chatter Communication

Deal Desk's primary communication channel is Salesforce Chatter. When you chatter `@Sales-Support`, it will automatically create a case in the Deal Desk queue. Deal Desk team members monitor the queue throughout the day and will respond to a case within 6 hours, Monday-Friday, with the exception of National/Regional holidays. Resolution or escalation will occur within 24 hours. 

To Chatter the DD team, tag `@Sales-Support`in Chatter on the related opportunity or account page and a short sentence on your request. If the Deal Desk team needs more information, we will follow up directly via Chatter.

**Please avoid tagging Deal Desk team members directly in chatter**, instead use @Sales-Support to ensure coverage in case the DD team member who replied first is unavailable. If someone is working on a case, they will continue to support until the case is closed.  If an issue has been resolved, please chatter @Sales-Support to reopen a case.

##### Slack Communication

##### Primary Slack Channel

Use our Slack channel in case of general, non-record related requests and/or urgent questions: 
**#sales-support** [If the request is related to a quote, opportunity, or acccount - please chatter on the page in Salesforce instead of the Slack channel.] 

##### Slack Best Practices

Please avoid contacting the DD team members directly via Slack. Utlizing the channel is best to ensure timely coverage and helps others who may have simliar questions.

In cae of a specific opportunity or quote related question please use SF Chatter (see section [Salesforce Chatter Commnunication](##### Salesforce Chatter Communication Basics))

##### Slack Announcements

Desk Desk process updates and announcements will be communicated via **#sales** and **#sales-support** Slack channels. 


##### Deal Desk Office Hours

Weekly Deal Desk Office Hours are scheduled each Wednesday at 12 PM EST. During Month End, Office Hours will take place on Monday,Wednesday, and Friday, scheduled in both AMER and EMEA time zones. Calendar invites will be sent to Sales-All Distribution group. Priority will be given to opportunities closing within the quarter. 

Supported topics include:
* Create or modifying a quote
* Quote approval acceleration
* IACV calculation
* Submitting an opportunity for close
* Validation/segmentation of closed opportunities
* And anything else to help drive opportunities closing within the quarter!

### IACV + Renewals

##### Calculating IACV 

To calculate IACV, please review the [IACV page of the handbook](/handbook/sales/#incremental-annual-contract-value-iacv). Alternatively, please chatter @Sales-support on the opportunity for assistance in calculating IACV. 

You can also use [this calculator](https://docs.google.com/spreadsheets/d/10hX1ZwTuxa-5PyJr30rTlATClzXmc8i0OunW1u-2D2I/edit#gid=0) to **estimate** the IACV for renewals.

##### Flat Renewal Support

**Please note that Flat Renewal Support has been decommissioned due to sever under utilization and inefficiencies in the process. To create a Flat Renewal quote, please review the Quote Configuration Guide below.

## Zuora Quote Configuration Guide


##### **1. New Subscription Quote**

*Training Video Placeholder - Coming in February*

A.  Open the New Business opportunity and click the **“New Quote”** button.

B.  When prompted **select “New Subscription”** and click “Next.”

C.  Provide Quote, Account, and Subscription Term Details and click “Next.”
*   **Select “Sold To” and “Bill To” contacts.** Note that the “Sold To” contact will receive the EULA and/or license key via email. Note: Each contact record must have a complete address.
*   For **Reseller deals,** populate the “Invoice Owner” and “Invoice Owner Contact” fields.
    *   Ensure that the Invoice Owner Contact is connected to the correct reseller account - i.e. if the reseller is in Germany, the Invoice Owner Contact should also be based in Germany.
    *   Reseller deals require that **“Click-through EULA required?” be set to “Yes.”**
*   Populate **“Initial Term”** in months. (i.e. for a two-year deal, enter “24”)
*   If the customer or reseller is based in the EU, enter the “VAT ID” number.
*   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date” field** should be populated with the date of the MSA’s signature. In addition, select the **“EXISTING AGREEMENT”** quote template.
*   Note: Non-standard payment terms (Net 30 = standard) should be reflected on the quote object, and will require approval per the approval matrix.

D.  Select **“Add Base Products”** from the drop-down menu on the “Edit Products and Charges” page. Select the correct product and click Next.
*   Note: Choose the correct term length - i.e. a 2 year Premium deal should use “Premium - 2 Year”
E.  Enter the product quantity, and adjust the discount or effective price as needed. Click Submit.
F.  **Order Form Generation**
*   If no discounts or special terms are requested, click “Generate PDF.”
*   If the deal contains discounts that require approval, please submit the quote for approval using the button on the quote. The quote must be approved before the PDF can be generated.
    *   In the case of reseller deals, please obtain the approvals in Chatter based on the approval matrix.
*   **Order Form Manual Edits**
    *   Please note that Sales reps may only generate PDF versions of the Order Form. If you require special wording or other manual edits, please make the request by tagging @Sales-Support in chatter.

##### **2. Amend Subscription Quote**

*Training Video Placeholder - Coming in February*

Note: This quote type should be used when new users are being added to an existing subscription. This includes both additional licenses to existing products, and true-ups. This also includes any scenario where the products are being changed during the term - i.e., an upgrade from Premium to Ultimate.

**A.  Add-On Quote Creation**
*   Open the New Business opportunity and click the **“New Quote”** button.
*   Select the **existing billing account**.
*   When prompted **select “Amend Existing Subscription for this Billing account**, and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Provide Quote, Account, and Subscription Term Details and click “Next.”
    *   The **Start Date** cannot be set before the subscription start date.
    *   The **End Date** will automatically be set to co-terminate with the existing subscription.
    *   The **Initial Term** should match the initial term of the New Business or Renewal subscription that precedes this quote. (i.e. if you’re amending the quote halfway through a 12 month term, the Initial Term should be 12, not 6.
    *   The **GitLab Entity** must be the same as it was on the initial deal you’re amending.
    *   For **Reseller deals,** populate the “Invoice Owner” and “Invoice Owner Contact” fields.
        *   Ensure that the Invoice Owner Contact is connected to the correct reseller account - i.e. if the reseller is in Germany, the Invoice Owner Contact should also be based in Germany.
        *   Reseller deals require that **“Click-through EULA required?” be set to “Yes.”**
    *   If the customer or reseller is based in the EU, enter the “VAT ID” number.
    *   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date”** field should be populated with the date of the MSA’s signature. In addition, select the **“EXISTING AGREEMENT”** quote template.
    *   Note: Non-standard payment terms (Net 30 = standard) should be reflected on the quote object, and will require approval per the approval matrix.
*   Under the **Edit Products and Charges** page, increase the existing license quantity to reach your total - i.e. enter the new total license count. This page will show all currently licensed products (marked “Original”), but the Order Form that generates will only show the added quantity and amount for the pro-rated period. **To add users to the existing subscription at a different price**, the new user licenses should be added on a separate product line. 
*   **True-Ups:** If you are quoting true-up users, click “Add Products” and wait for the next page to load. Then, click “Select” and choose “Add Add-On Products.” Select True-Up and click Next. Edit the quantity and effective price. Click “Submit.”
*   **Order Form Generation**
    *   If no discounts or special terms are requested, click “Generate PDF.”
    *   If the deal contains discounts that require approval, please submit the quote for approval using the button on the quote. The quote must be approved before the PDF can be generated.
        *   In the case of reseller deals, please obtain the approvals in Chatter based on the approval matrix.
    *   **Order Form Manual Edits**
        *   Please note that Sales reps may only generate PDF versions of the Order Form. If you require special wording or other manual edits, please make the request by tagging @Sales-Support in chatter.

**B.  Upgrade or Switch Products During the Subscription Term**
*   Create an “Amend Subscription” quote by following the steps in Section 2 (A) above.
    *   The **Start Date** should be the date of the product exchange. 
    *   On the **Edit Products and Charges** page, select “Add Products” and wait for the next page to load.
    *   Click “Select” and click “Add Base Products.”
    *   Select the **new** product type, and the correct SKU. Click Next.
    *   Select the **Remove** drop down button (which is not fully visible) next to the current product, which you are removing in lieu of the new product.
    *   Adjust the new product line - quantity, discount. Click “Submit.”
    *   Note: On the Order Form, the product being removed will display with a negative amount reflecting the credit for that product for the remainder of the subscription term.

##### **3. Renew Subscription Quote**

*Training Video Placeholder - Coming in February*

**A.  Standard Renewal**
*   Open the Renewal opportunity and click the **“New Quote”** button.
*   Select the **existing billing account.**
*   When prompted **select “Renew Existing Subscription for this Billing account,** and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Provide Quote, Account, and Subscription Term Details and click “Next.”
    *   The **Start Date** cannot be edited. This will be the true renewal date.
    *   The **End Date** will automatically be determined by the Renewal Term.
    *   Populate **“Renewal Term”** in months. (i.e. for a two-year renewal, enter “24”)
    *   Select the proper **GitLab Entity.**
    *   For **Reseller** deals, populate the **“Invoice Owner”** and **“Invoice Owner Contact”** fields.
        *   Ensure that the Invoice Owner Contact is connected to the correct reseller account - i.e. if the reseller is in Germany, the Invoice Owner Contact should also be based in Germany.
        *   Reseller deals require that **“Click-through EULA required?” be set to “Yes.”**
    *   If the customer or reseller is based in the EU, enter the **“VAT ID”** number.
    *   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date”** field should be populated with the date of the MSA’s signature. In addition, select the **“EXISTING AGREEMENT” quote template.**
    *   Note: Non-standard payment terms (Net 30 = standard) should be reflected on the quote object, and will require approval per the approval matrix.
*   Click Next, to enter the Products and Charges page.
    *   Note: The product lines from the Initial Term are already listed and will be marked “Original.” You may add new products, update the quantity on the original license, or remove the existing license. To add users to an existing license at a different price, the new user licenses should be added on a new product line.

**B.  Renewal Using a New Subscription Quote**

Note: Certain renewals require a New Subscription quote. The most common scenario is if you are changing the term as of the renewal date - i.e. changing from an annual plan to a multi-year plan.

*   Open the Renewal opportunity and click the **“New Quote”** button.
*   Select the **existing billing account.**
*   When prompted **select “New Subscription for this Billing account,”** and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Fill out the necessary information on the quote per the instruction under the **New Subscription Quote** section above. 
    *   **Note: The Start Date must be the renewal date.** 
*   Click Next and update the products and fees per the steps above.

**C.  Contract Reset**

Note: Contract Resets are used to perform an "Early Renewal" - i.e. start a new 12 month subscription before the renewal date.

*   Open the Renewal opportunity and click the **“New Quote”** button.
*   Select the **existing billing account.**
*   When prompted **select “New Subscription for this Billing account,”** and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Fill out the necessary information on the quote per the instruction under the New Subscription Quote section above. 
    *   **Note: The Start Date should be the new subscription term’s start date, or the “Early Renewal” date.**
*   Click Next and update the products and fees per the steps above.
*   **IMPORTANT:** Next, tag @Sales-Support in Chatter on the Renewal Opportunity to create a credit opportunity and quote to cancel the existing subscription, which in this scenario is being replaced with the new subscription. Deal Desk will then manually generate an Order Form to add the credit line into the contract.
*   Send the order form to the customer for signature.
*   Upload the signed order form to the renewal opp and to the credit opp as well and submit both opps for approval.

**D.  Add-On + Early Renewal**

*   Create an add-on quote per the Add-On section above.
*   Create a Renewal Quote object from the renewal opportunity per the Renew Subscription section above.
*   **IMPORTANT:** Tag @Sales-Support in Chatter on the opportunity to create the consolidated Order Form.

##### **4. Professional Services Quote**

*Training Video Placeholder - Coming in February*

**A.  Create a standalone Professional Services Opportunity.**

*   Create a New Business Opportunity and select “Professional Services Only” under “Opportunity Record Type.” **Note: Professional Services products must always exist on a separate, standalone opportunity.**

**B.  Creating a Professional Services Quote with Standard Product SKUs**

*   Create a new subscription quote under the Professional Services opportunity by following the steps above under “new Subscription Quote.” Select the required SKUs for Professional Services.
*   More information on [Professional Services SKUs](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/#professional-services-skus) 

**C.  Creating a Professional Services Quote with Custom SKUs**

*   Create a new subscription quote under the Professional Services opportunity by following the steps above under “new Subscription Quote.” Select **GitLab Service Package.** Update the price to reflect the price on the SOW.

**D.  Before submitting a Professional Services Opportunity for Closure:**

*   Please note that the following items must be attached to the opportunity before it can be Closed Won:
    *   SOW signed by **both the customer and Paul Machle.****
    *   Cost Estimate spreadsheet (Link provided by the PS team)

##### **5. Multi-Year Deals**

*Training Video Placeholder - Coming in February*

A.  Note: In the case of multi-year deals, the Initial Term” should be updated to reflect the number of months in the term - 24, 36, 48, etc. Also, select the correct Product (i.e. for a 2 year Starter deal, select “Starter - 2 Year”).

B.  If annual payments are requested for multi-year deals, use the 1 Year product. Note that annual payments must be approved in chatter by Paul Machle.

C.  Note: If annual payments are approved, create a separate opportunity and quote for each year of the subscription. A three-year deal with an annual payment schedule will have 3 separate opportunities and quotes reflecting each year of the subscription. Work with @Sales-Support in Chatter if needed

##### **6. Ramp Deals**

*Training Video Placeholder - Coming in February*

A.  To create a ramp deal, tag @Sales-Support in chatter on the opportunity. **Provide the following information for each ramp period:**
*   Start date/term length
*   Bill To and Sold To Contact
*   Product, quantity, discount
*   Payment Terms (i.e. Net 30)
*   Any other requests (i.e. Price Lock)

B.  Request template:
*   Deal Desk has created a [G Sheet template](https://docs.google.com/spreadsheets/d/1ho_ndKIZDvgdWOn873XONK2oc1tLloVJwlnpxgHYjiE/edit#gid=0) for Sales reps to enter ramp information.
    *   To use this template, copy the template to your own Drive, enter the information, and share your G Sheet with @Sales-Support in Chatter.

C.  Note: Deal Desk will create the quotes and Order Form.

##### **7. Miscellaneous (But Important) Information**

*Training Video Placeholder - Coming in February*

A.  **To add users to an existing license at a different price**, please add the users on a new, separate product line.

B.  **To create a true-up/add-on quote for a multi-year deal**, please add **both** the true-up and increase the license count by the same number of users. Note that the user number cannot decrease during the term of a multi-year deal - i.e. in the case of a three-year deal, if the customer exceeds the 100 license count by 25 users, (1) True-Up SKU with 25 users, and (2) increase the license quantity from 100 to 125.

C.  **If the customer signs a renewal quote, but a true-up is required before the renewal date**, create an add-on opportunity from the closed renewal opportunity, use the same start date as the renewal, and add the necessary true-up.

D.  If you have **multiple quote objects** under one opportunity, the quote you are using **must be marked Primary.**

## Month End Close Process

*Note: The below Google Doc is a placeholder, and the process will be merged onto this page from the Google Doc later in February*

The Deal Desk team will assist with Month End Close Process tasks. For a detailed overview of the tasks and responsibilities of Month End, please review [this document](https://docs.google.com/document/d/1Sn-w7a6M91GF9jnt2-5eHuYG3ID6B48qNWZPhI1LKYU). 



