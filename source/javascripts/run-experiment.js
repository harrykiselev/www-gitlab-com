// Define the namespace
var runExperiment = function(issueId, featureFlag, experimentControlExtraFunctions, experimentTestExtraFunctions)
{
  // Define targets
  var experimentWrapperId = '#experiment' + issueId;
  var experimentTargetControl = experimentWrapperId + ' .experiment-control';
  var experimentTargetTest = experimentWrapperId + ' .experiment-test';
  var experimentLoading = experimentWrapperId + ' .loading-experiment';
  var controlExtras = experimentControlExtraFunctions;
  var testExtras = experimentTestExtraFunctions;

  // Initialize the user object
  var user = {'anonymous': true};
  // Initialize LaunchDarkly
  var ldclient = LDClient.initialize('5e4333e30437cc080f58513a', user);

  // Define what should happen
  function render()
  {

    // Init the feature flag object
    var experimentIsEnabled = ldclient.variation(featureFlag, false);

    // Show, hide, and run extra functions.
    if(experimentIsEnabled)
    {
      // THIS IS THE TEST VARIANT

      // TIMEOUTS: wait 150 ms to poll experimentIsEnabled
      setTimeout(function()
      {
        document.querySelector(experimentLoading).classList.add('loaded-experiment');
        document.querySelector(experimentTargetControl).classList.remove('experiment-visible');
        document.querySelector(experimentTargetTest).classList.add('experiment-visible');
      }, 150);

      if(experimentTestExtraFunctions !== null)
      {
        // TIMEOUTS: wait another 200 ms for the fade animation to finish (total 350ms)
        setTimeout(function()
        {
          testExtras();
          //console.log('test extras: ' + testExtras);
        }, 350);
      };

    } else {
      // THIS IS THE CONTROL VARIANT

      // TIMEOUTS: wait 100 ms to poll experimentIsEnabled
      setTimeout(function()
      {
        document.querySelector(experimentLoading).classList.add('loaded-experiment');
        document.querySelector(experimentTargetTest).classList.remove('experiment-visible');
        document.querySelector(experimentTargetControl).classList.add('experiment-visible');
      }, 150);

      if(experimentControlExtraFunctions !== null)
      {
        // TIMEOUTS: wait another 200 ms for the fade animation to finish (total 350ms)
        setTimeout(function()
        {
          controlExtras();
          //console.log('control extras: ' + controlExtras);
        }, 350);
      };
    };
  };
  // Subscribe to the feature flag and then run the functions based on it's state
  ldclient.on('ready', render);
  ldclient.on('change', render);
};
